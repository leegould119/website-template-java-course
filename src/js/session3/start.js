let sessionNumber = "sesssion3 javascript";
console.log(sessionNumber);

const mainHeading = document.getElementById("session-number");
mainHeading.textContent = sessionNumber;

const testButton = document.getElementById("test-button");
testButton.addEventListener("click", function () {
  // const output = document.getElementById("output");
  // const outputText = document.createElement("p");
  // output.appendChild(outputText);
  // outputText.textContent = ('button clicked.');

  // forLoop();
  // doWhileLoop();
  //whileLoop();

  // arrayTest();
  generateLayout();
  // getUserInfo();
  arrayTest();
});

// submit function
const submitButton = document.getElementById("submit-button");
submitButton.addEventListener("click", function () {
  console.log("working ");
  postUserInfo();
});

// post request
function postUserInfo() {
  const firstName = document.getElementById("firstName").value;
  const lastName = document.getElementById("lastName").value;
  const email = document.getElementById("email").value;
  const telNumber = document.getElementById("telephoneNumber").value;

  console.log("first name value : " + firstName);
  console.log("last name value : " + lastName);
  console.log("email value : " + email);
  console.log("telephone number value : " + telNumber);

  let user = {
    firstName: firstName,
    lastName: lastName,
    email: email,
    telephoneNumber: telNumber
  };

  console.log(JSON.stringify(user));
}

// get trequest
function getUserInfo() {
  // let car = new Object();
  // let car = {};
  let contact = {
    firstName: "Lee",
    lastName: "Gould",
    email: "leegould@email.com",
    tel: "0730200806",
    adress: {
      street: "street name",
      city: "city",
      country: "country",
      postCode: "postCode"
    }
  };

  console.log("contact" + JSON.stringify(contact));
  console.log("contact : " + contact);

  const firstNameInput = document.getElementById("firstName");
  firstNameInput.value = contact.firstName;

  const lastNameInput = document.getElementById("lastName");
  lastNameInput.value = contact.lastName;

  const emailInput = document.getElementById("email");
  emailInput.value = contact.email;

  const telephoneNumberInput = document.getElementById("telephoneNumber");
  telephoneNumberInput.value = contact.tel;
}

// were going to create out functions here...

// for loops
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration

// for objects
// https://www.w3schools.com/js/js_objects.asp

// for arrays
// https://www.w3schools.com/js/js_arrays.asp

// for loop

function forLoop() {
  // let variable step
  // number = 0
  // step less that 5 (step < 5)
  // step ++ (increment step)
  for (let step = 0; step < 15; step++) {
    // Runs 5 times, with values of step 0 through 4.
    console.log("Walking east one step " + step);

    const output = document.getElementById("output");
    const outputText = document.createElement("p");
    output.appendChild(outputText);
    outputText.textContent = "do while steps : " + step;
  }
}

// do while loop
function doWhileLoop() {
  let step = 0;
  // step = 0  (Operator "+=" : step  = step + 1)
  do {
    step += 1;
    // console.log("do while steps : " + step);
    const output = document.getElementById("output");
    const outputText = document.createElement("p");
    output.appendChild(outputText);
    outputText.textContent = "do while steps : " + step;
  } while (step < 5);
}

// while loop
function whileLoop() {
  let step = 0;
  let x = 0;
  while (step < 3) {
    // increment step
    step++;
    // x
    // +=  0 = (x = 0) + (step = 3 );
    x += step;
    console.log("step : " + step + " x " + x);
  }
}

function arrayTest() {
  // let array = new Array();
  let names = [
    "James",
    "Bob",
    "Garry",
    "Bjorn",
    "Pernilla",
    "Nathalie",
    "Sandra"
  ];

  let numbers = [1, 2, 3, 4, 5, 6, 7];

  console.log("names in the array : " + names);

  // added a new name steve
  names.push("Steve");
  console.log("added a new name : " + names);

  names.pop();
  console.log("removed a name from the end of the array : " + names);

  names.shift();
  console.log("removed a name from the brginning of the array : " + names);

  names.unshift("Jerry");
  console.log("adds a name to the brginning of the array : " + names);

  // console.log("names in the array : " + numbers);

  // console.log("how long is the array : " + names.length);

  // // index value of an array always starts at 0
  // console.log("specific name in the array : " + names[0]);
  // // read more about arrays
  // // https://www.w3schools.com/js/js_arrays.asp
  // // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Arrayhttps://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array

  // names.forEach(function (item, index) {
  //     console.log(" item : " + item + " index : " + index);

  // });
}

function generateLayout() {
  let sections = [
    "about",
    "services",
    "contact",
    "resume",
    "portfolio",
    "info",
    "random",
    "projects",
    "media"
  ];

  let mainContainer = document.getElementById("test");

  sections.forEach(function (item) {
    const section = document.createElement("section");
    // we have create a attribute id
    let attr = document.createAttribute("id");
    attr.value = item;
    section.setAttributeNode(attr);

    // we will create a attribute clas
    // H1 ELEMENT
    let heading = document.createElement("h1");
    heading.textContent = item;
    section.appendChild(heading);
    mainContainer.appendChild(section);

    if (item === "info") {
      let attrClass = document.createAttribute("class");
      attrClass.value = "row  light-orange section-wrapper";
      section.setAttributeNode(attrClass);

      // class container
      let wrapper = document.createElement("div");
      let divAttrib = document.createAttribute("class");
      divAttrib.value = "container";
      wrapper.setAttributeNode(divAttrib);
      section.appendChild(wrapper);

      //create columns
      let colums = [1, 2, 3, 4];
      colums.forEach(function (item) {
        const cols = document.createElement("div");
        let attrCols = document.createAttribute("class");
        attrCols.value = "col-3";
        cols.setAttributeNode(attrCols);
        wrapper.appendChild(cols);
      });
    } else if (item === "random" || item === "portfolio") {
      let attrClass = document.createAttribute("class");
      attrClass.value = "row dark-grey section-wrapper";
      section.setAttributeNode(attrClass);
    } else {
      let attrClass = document.createAttribute("class");
      attrClass.value = "row section-wrapper";
      section.setAttributeNode(attrClass);
    }
  });
}

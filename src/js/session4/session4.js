let sessionNumber = "session4 Javascript";

const header = document.getElementById("session-number");
header.textContent = sessionNumber;

// get the button to create a click event

const submitButton = document.getElementById("create-session");
submitButton.addEventListener("click", function (event) {
  event.preventDefault();
  // console log as a test

  // form elements
  const userName = document.getElementById("user-name").value;
  const firstName = document.getElementById("first-name").value;
  const lastName = document.getElementById("last-name").value;

  setSession(userName, firstName, lastName);
});

// create session storage (experiment)

let setSession = function setSessionStorage(userName, firstName, lastName) {
  //   console.group("FORM INPUTS");
  //   console.log("user name : " + userName);
  //   console.log("first name : " + firstName);
  //   console.log("last name : " + lastName);
  //   console.groupEnd();

  //   setting the session state.
  // sessionStorage.setItem("key", "value");

  let userData = new Object();

  userData = {
    userName: userName,
    firstName: firstName,
    lastName: lastName
  };

  sessionStorage.setItem("userData", JSON.stringify(userData));
};

const getInfo = document.getElementById("get-info");
getInfo.addEventListener("click", function () {
  console.log("get info");
  getSession();
});

// getting a session item

let getSession = function getSessionStorage() {
  // sessionStorage.geetItem('key');
  let userDataString = sessionStorage.getItem("userData");

  let userData = JSON.parse(userDataString);

  console.log("getting session storage : " + userData.lastName);

  //   getting the input and addind the session field to it
  let userNameInput = document.getElementById("user-name-session");
  userNameInput.value = userData.userName;

  let userFirstNameInput = document.getElementById("user-first-name-session");
  userFirstNameInput.value = userData.firstName;

  let userLastNameInput = document.getElementById("user-last-name-session");
  userLastNameInput.value = userData.lastName;
};

const deleteInfo = document.getElementById("delete-info");
deleteInfo.addEventListener("click", function () {
  deleteSession();
});

let deleteSession = function deleteSessionStorage() {
  console.log("delete session storage");
  let confirmMessage = document.getElementById("confirm");
  confirmMessage.textContent = "deleted";
  sessionStorage.removeItem("userData");
};

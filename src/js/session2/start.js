
// making sure this file works with a test console log

let sessionNumber = "sesssion2 javascript";
console.log(sessionNumber);

// variable is a simple container that you can assign a value to

// strings "text"
// number 1-0 
// boolean true or false
// decimal number 1.1


// 2 typers of variables
// let (you can change the value of this variable)
// const (it's static, can't change the value)

// var is not advisable

const mainHeading = document.getElementById('session-number');
mainHeading.textContent = (sessionNumber);
console.log(typeof mainHeading);


// setting up the button 
const testButton = document.getElementById('test-button');

let clicked = 1;
// test the varible
console.log(typeof clicked);

testButton.addEventListener('click', function () {

    clicked++;

    // Inccrement ++
    // Decriment --

    // https://www.w3schools.com/js/js_operators.asp

    console.log("this button was clicked " + clicked + " times.");

    // div element with id output
    const output = document.getElementById("output");
    const outputText = document.createElement("p");

    output.appendChild(outputText);
    // add text to the p tag


    // mathermatical calculation
    // addition
    let test1 = clicked + 10;
    let test2 = clicked * 10;


    // outputText.textContent = (clicked + ' + 5  = ' + test1);


    if (clicked > 10) {
        //if clicked 10 or less do this
        outputText.textContent = (' clicked is ' + true + ' clicked value : ' + clicked);

    } else if (clicked === 12) {
        // if ther item is clicked 12 times do this
        outputText.textContent = (' item clicked ' + clicked + ' times. ');
    } else {
        // otehrwise do this
        // outputText.textContent = (' Added a new element ' + clicked + ' times. ');
        outputText.textContent = (clicked + ' + 10  = ' + test1);
    }
});
console.log(typeof testButton);



// let clicked = 0;

// testButton.addEventListener('click', function () {

//     clicked++;
//     hello = "new paragraph element is dynamically created";
//     console.log('buttton works');

//     const output = document.getElementById('output');
//     const para = document.createElement("p");

//     output.appendChild(para);

//     para.textContent = hello + " " + clicked + " times";

// });


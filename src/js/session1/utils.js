// starting javascript basics


// get the document tag 

// static string ('Hello world')
// document.getElementById('javascript-demo').textContent = 'Hello world';


// value = variable;  variable type = string

// function changeHeaderText(value, name) {
// let message = value + " " + name;
// document.getElementById('javascript-demo').textContent = value + " " + name;
// }

function changeHeaderText(value) {
    document.getElementById('javascript-demo').textContent = value;
}
// get button 
const button = function bannerButton(value) {
    // document.getElementById("the id of the element")
    // event listner which listens for a click which is a function
    // https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener

    document.getElementById('event-btn').addEventListener("click", function () {
        // logging out a message to the console.
        console.log(value);

        // value equal to 
        if (value == "10") {
            console.log("always true if value is equal");
        }

        // value and type equal to 
        if (value === "10") {
            console.log(true);
        }
        else {
            console.log(false);
        }

        const number = 10;

        // value and type not equal to 
        if (value === number) {
            console.log(true);
        }
        else {
            console.log(false);
        }

        // < less than
        // > greater than
        // >= greater than or equal to 
        // <= less than or qeual to 
        const test2 = 15;
        if (value <= test2) {
            console.log("test that 10 > 15 " + false);
        } else {
            console.log(true);
        }

        // https://www.w3schools.com/js/js_operators.asp
        // https://www.w3schools.com/js/js_variables.asp
    });
};




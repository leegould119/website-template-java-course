/*
    STARTING JS
    good javascript resources at
    https://developer.mozilla.org/

    for homework start to read
    https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript

*/

console.log("Hello world!");

// don't use variable var, bad practise
// var welcomeMessage = "Welcome to javascript";

// string 
// let keyword change the value of the variable
let welcomeMessage = "Welcome to my site";

welcomeMessage = "Welcome";

// const (constant)
// do not change 

const name = "Lee";
// changeHeaderText(welcomeMessage, name);
let message = `Hello ${name}`;

//number does not have to not have to be wrappen quotes
// number 
let number = 10;

// more on numbers 
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number

//integer
let integer = 2.2;

// boolean
let boolean = true;
// true or false
changeHeaderText(message);


// example of altenative method of setting variables
// const string = "test",
//     number = 20,
//     bool = true;

/* 
------------------------------------------------------------------
if statements
if else statements
*/
const test = 10;
button(test);
















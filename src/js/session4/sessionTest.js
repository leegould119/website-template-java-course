const header = document.getElementById("sessionNumber");
header.textContent = "session4 Javascript";

let userDataString = sessionStorage.getItem("userData");

let userData = JSON.parse(userDataString);
console.log("getting session storage : " + userData);

// get the paragraph tag and the a tag
let welcomeMessage = document.getElementById("welcome-message");
let userLink = document.getElementById("link");

if (userData === null || userData === undefined) {
  console.log("no user data" + userData);
  welcomeMessage.textContent = "please fill in your user data";

  userLink.classList.remove("invisible");
  userLink.classList.add("visible");
} else {
  console.log("userData" + userData);
  welcomeMessage.textContent =
    "Welcome back " + userData.firstName + " " + userData.lastName;
  userLink.classList.remove("visible");
  userLink.classList.add("invisible");
}

// here is some reading
// https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
// https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage

// here is a practical assignment
// https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API

let sessionNumber = "sesssion4 javascript";
console.log(sessionNumber);

const mainHeading = document.getElementById("session-number");
mainHeading.textContent = sessionNumber;

// add button
const addButton = document.getElementById("add-button");
addButton.addEventListener("click", function (e) {
  e.preventDefault();
  let username = document.getElementById("Username").value,
    value = "username";

  setSessionStorage(value, username);
});

// set a newession storage
function setSessionStorage(name, value) {
  sessionStorage.setItem(name, value);
  let storageItem = sessionStorage.getItem(name);
  console.log("storageItem : " + storageItem);
  let printval =
    "adding a new session storage item  : " + name + " : " + storageItem;
  printToScreen(printval);
}

// delete button
const deleteButton = document.getElementById("delete-button");
deleteButton.addEventListener("click", function (e) {
  e.preventDefault();
  let value = "username";
  deleteSessionStorage(value);
});

// delete session storage
function deleteSessionStorage(value) {
  let printval = "removing session storage item : " + value;
  printToScreen(printval);
  sessionStorage.removeItem(value);
}

function printToScreen(value) {
  let output = document.getElementById("output");
  output.textContent = value;
}
